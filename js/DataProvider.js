/**
 * Manages fetching and storing of TaskLists on a back-end server
 */
class DataProvider {

    /**
     * URL of the backend
     * @return {string}
     */
    static get URL() {
        return 'http://zhaw.herokuapp.com/task_lists/';
    }

    /**
     * Loads the specified TaskList from the server
     * @param identifier
     * @returns {Promise<TaskList>} The loaded TaskList
     */
    static load(identifier) {
        return fetch(this.URL + identifier)
            .then(response => response.json())
            .then(data => TaskList.unserialize(data))
    }

    /**
     * Saves a TaskList on the server
     * @param taskList
     * @returns {Promise<number>} The TaskList identifier (will be generated if the TaskList didn't have one earlier
     */
    static save(taskList) {
        const requestBody = taskList.serialize()
        let url = this.URL;
        if (taskList.identifier) {
            url += taskList.identifier
        }

        // I cannot set the content-type header to json as it would cause a preflight OPTIONS request to be made.
        // The server (zhaw) will return 404 on this and will fail the whole request.
        return fetch(url, {
            body: JSON.stringify(requestBody),
            method: 'POST',
            // headers: {
            //     'content-type': 'application/json'
            // }
        })
            .then(response => response.json())
            .then(response => {
                taskList.identifier = response.id
                return response.id
            })
    }

}
