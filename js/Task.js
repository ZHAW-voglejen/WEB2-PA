/**
 * Describes a single task that can either be done or pending
 */
class Task {

    constructor(name) {
        this.name = name
        this.isDone = false
    }

    getName() {
        return this.name
    }

    getState() {
        return this.isDone
    }

    toggleState() {
        this.isDone = !this.isDone
    }

    setState(state) {
        this.isDone = state
    }

    /**
     * Renders an HTML representation of the task
     * The inputs are bound to this task and auto-update
     *
     * @returns {jQuery|HTMLElement}
     */
    render() {

        const name = this.name.replace(/[^a-zA-Z0-9]+/g, '');
        const id = `cb_${name}`

        const $wrapper = $("<li>", {
            class: 'list-group-item task'
        })

        const $wrapper2 = $("<div>", {
            class: 'form-check'
        })

        const $checkBox = $("<input>", {
            class: 'form-check-input',
            type: 'checkbox',
            name: name,
            id: id
        })

        $checkBox.prop('checked', this.isDone)
        $checkBox.on('change', () => {
            this.isDone = $checkBox.prop('checked')
            if (this.isDone) {
                $wrapper.addClass('list-group-item-dark')
            } else {
                $wrapper.removeClass('list-group-item-dark')
            }
        })
        $checkBox.trigger('change')

        const $input = $("<input>", {
            type: 'text',
            class: 'form-control'
        })
        $input.attr('placeholder', 'Unbennant')
        $input.val(this.name)
        $input.on('keyup change', () => this.name = $input.val())

        $wrapper2
            .append($checkBox)
            .append($input)

        $wrapper.append($wrapper2)

        return $wrapper
    }
}