/**
 * Manages a list of tasks
 */
class TaskList {

    constructor(name, identifier) {
        this.tasks = [];
        this.name = name;
        this.identifier = identifier;
    }

    add(task) {
        this.tasks.push(task)
    }

    getTasks() {
        return this.tasks
    }

    /**
     * Renders all tasks in a list
     *
     * @returns {jQuery|HTMLElement}
     */
    render() {

        const $wrapper = $("<ul>", {
            class: 'list-group'
        })

        this.tasks.forEach(task => $wrapper.append(task.render()))

        return $wrapper
    }

    /**
     * Serializes all the information in this TaskList to a JS Object
     *
     * @returns {{title: string, identifier?: string, tasks: Array<{title: string, done: boolean}>}}
     */
    serialize() {
        let data = {
            title: this.name,
            tasks: []
        }

        if (this.identifier) {
            data.id = this.identifier
        }

        console.log(this.tasks, this.tasks[0])
        for (const t of this.tasks) {
            console.log(t)
            data.tasks.push({
                title: t.getName(),
                done: t.getState()
            })
        }

        return data;
    }

    /**
     * Unserializes a JS Object to a TaskList
     *
     * @param {{title: string, identifier?: string, tasks: Array<{title: string, done: boolean}>}} data
     * @returns {TaskList}
     */
    static unserialize(data) {
        console.log(data)
        const list = new TaskList(data.title, data.id)

        for (const taskObj of data.tasks) {
            const task = new Task(taskObj.title ? taskObj.title : '')
            task.setState(taskObj.done)
            list.add(task)
        }

        return list
    }

}