describe("TaskList", function () {

    // Set up test data-provider
    let storedData = {
        'test': {
            id: 'test',
            title: 'Testing',
            tasks: [
                {
                    title: 'Test Task',
                    done: false
                }
            ]
        }
    }

    beforeEach(function () {
        spyOn(DataProvider, 'load').and.callFake(function (identifier) {
            return new Promise((resolve, reject) => {
                if (identifier in storedData) {
                    resolve(TaskList.unserialize(storedData[identifier]))
                } else {
                    reject('No task for identifier')
                }
            })
        })

        spyOn(DataProvider, 'save').and.callFake(function (taskList) {
            return new Promise((resolve) => {
                if (!taskList.identifier) {
                    taskList.identifier = Math.floor((Math.random() * 1000) + 1)
                }

                storedData[taskList.identifier] = taskList.serialize()
                resolve(taskList.identifier)
            })
        })
    })

    it("can be created", function () {
        const list = new TaskList()

        expect(list).toBeDefined()
    })

    it("can have tasks added to it", function () {
        const list = new TaskList()

        list.add(new Task("Hello"))
        list.add(new Task("Bye"))
        list.add(new Task("Wooo"))

        expect(list.getTasks().length).toBe(3)
    })

    it("renders all tasks", function () {
        const list = new TaskList()

        list.add(new Task("Hey"))
        list.add(new Task("Bye"))

        expect(list.render().find("li").length).toBe(2)
    })

    it("can be stored and restored", function (done) {

        const list = new TaskList()
        const task = new Task('Hey')
        list.add(task)

        spyOn(list, 'serialize').and.callThrough()
        spyOn(TaskList, 'unserialize').and.callThrough()

        DataProvider.save(list)
            .then(id => DataProvider.load(id))
            .then(newList => {

                expect(list.serialize).toHaveBeenCalled()
                expect(TaskList.unserialize).toHaveBeenCalled()

                expect(newList).toBeDefined()
                expect(newList.getTasks().length).toBe(1)
                expect(newList.getTasks()[0].getName()).toBe('Hey')

                done()
            })
            .catch((e) => {
                expect(true).toBe(false) // Jasmine has no nice way to handle promises...
                done(e)
            })





    })

})