describe("Task", function () {

    it("can be created", function () {
        const task = new Task("Test")
        expect(task).toBeDefined()
    })

    it("remembers it's name", function () {
        const task = new Task("Test")
        expect(task.getName()).toBe("Test")
    })

    it("stores it's state", function () {
        const task = new Task('Test')
        task.setState(true)
        expect(task.getState()).toBe(true)
    })

    it("can render an html node which contains a name and a checkbox", function () {
        const taskName = "Test"

        const task = new Task(taskName)
        const html = task.render() // Should return jQuery object

        expect(html.find("input[type=checkbox]").length).toBeGreaterThanOrEqual(1)
        expect(html.find("*:contains(" + taskName + ")"))
    })

});